var HarmonyERC20 = artifacts.require("HarmonyERC20");

const myAddress = "0x7c41e0668b551f4f902cfaec05b5bdca68b124ce";

const testAccount = "0xebcd16e8c1d8f493ba04e99a56474122d81a9c58";

const transferAmount = 2000000;

module.exports = function() {
    async function getHarmonyERC20Information() {
        let instance = await HarmonyERC20.deployed();
        let name = await instance.name();
        let total = await instance.totalSupply();
        let decimals = await instance.decimals();
        let mybalance = await instance.balanceOf(myAddress);
        instance.transfer(testAccount, transferAmount);
        let testAccBalance = await instance.balanceOf(testAccount);

        console.log("HarmonyERC20 is deployed at address " + instance.address);
        console.log("Harmony ERC20 Information: Name    : " + name);
        console.log("Harmony ERC20 Information: Decimals: " + decimals);
        console.log("Harmony ERC20 Information: Total   : " + total.toString());
        console.log("my address : " + myAddress);
        console.log("test account address : " + testAccount);
        console.log("my minted    H2O balance is: " + mybalance.toString());
        console.log("test account H2O balance is: " + testAccBalance.toString());
    }
    getHarmonyERC20Information();
};